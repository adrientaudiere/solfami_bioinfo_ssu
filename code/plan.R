plan <- drake_plan(
  data = list_fastq_files(path=file_in("data/raw_seq")),
  ## Quality graph before any analysis
  # qualGraph_fwd = target(plotQualityProfile(data$fnfs), peripheric = TRUE),
  # qualGraph_rev = target(plotQualityProfile(data$fnrs), peripheric = TRUE),
  # Pre-filtered data with low stringency
  filtered_data = target(
    filterAndTrim(fwd = data$fnfs, rev = data$fnrs,
                  filt = file_out("output/filterAndTrim_fwd"),
                  filt.rev = file_out("output/filterAndTrim_rev"),
                  trimLeft = 25, trimRight = 25, multithread = 1,
                  minLen = 50, maxEE = Inf, truncQ = 10, compress = TRUE)),
  # Dereplicate fastq files
  derep_fs = target(derepFastq(file_in("output/filterAndTrim_fwd")),
                    format = "qs"),
  derep_rs = target(derepFastq(file_in("output/filterAndTrim_rev")),
                    format = "qs"),
  # Learn Error rate for dada
  err_fs = target(learnErrors(derep_fs, multithread = 4), format = "qs"),
  err_rs = target(learnErrors(derep_rs, multithread = 4), format = "qs"),
  # Make amplicon sequence variants
  ddF = target(dada(derep_fs, err_fs, multithread = 4), format = "qs"),
  ddR = target(dada(derep_rs, err_rs, multithread = 4), format = "qs"),
  # Merge paired sequences and make a table of ASV x Samples
  ### merged_seq_nopool = target(mergePairs(ddF_FALSE, derep_fs, ddR_FALSE, derep_rs,
  ###                       minOverlap = 10,
  ###                       returnRejects = TRUE), transform = ),
  merged_seq = target(mergePairs(ddF, ddR, derepF = derep_fs, derepR = derep_rs,
                                 minOverlap = 10, returnRejects = TRUE),
                      format = "qs"),
  seq_tab_seq_F = target(makeSequenceTable(ddF)),
  # Remove chimera and sequences < 200 bp length.
  seqtab_w_short_seq = target(removeBimeraDenovo(seq_tab_seq_F)),
  seqtab = seqtab_w_short_seq[, nchar(colnames(seqtab_w_short_seq)) >= 200],
  # Taxonomic
  tax_tab = target(taxo_from_seqtab(seqtab,
                                    db_path = "data/silva_128.18s.99_rep_set.dada2_GLOMEROMYCOTA_clean2.fa")[,-8]
                   ),
  # Create the phyloseq object for further analysis
  ## Load sample data and ASV table with corresponding samples names
  id_SFM_sam = target(read.delim(file_in("data/sam_data.csv"))$id_parcelle,
                         peripheric = TRUE),
  id_SFM_asv = target(unlist(
    regmatches(data$fnfs, regexec(pattern="SFM-r-(.*?)_S",
                                  data$fnfs))) [seq(2, length(data$fnfs)*2, by = 2)],
                         peripheric = TRUE),
  sam_tab = target(sample_data_matching(file = file_in("data/sam_data.csv"),
                                        names_of_samples = id_SFM_sam,
                                        condition = match(id_SFM_sam, id_SFM_asv),
                                        taxa_are_rows = T)),
  asv_tab = target(otu_table(rename_sample(seqtab, names_of_samples = id_SFM_asv,
                                           taxa_are_rows = TRUE), taxa_are_rows=FALSE)),
  ## Create the phyloseq object 'data_phyloseq' with
  ##   (i) table of asv,
  ##   ii) taxonomic table,
  ##   (iii) sample data and
  ##   (iv) references sequences
  data_phyloseq = dada_to_phyloseq(asv_tab, sample_data(sam_tab), tax_tab),
  data_phyloseq_otu = asv2otu(data_phyloseq),
  data_phyloseq_otu_vsearch = asv2otu(data_phyloseq, method = "vsearch"),
  report_bioinfo = wflow_publish(knitr_in("analysis/Bioinfo.Rmd"), verbose = TRUE),
  report_ecology = wflow_publish(knitr_in("analysis/Ecology.Rmd"), verbose = TRUE),
  trace = TRUE
)

